# Bulbul - Jekyll theme based on premium ghost theme [Oak](https://creativemarket.com/stefan.djokic/168443-Oak-Clean-Responsive-Ghost-Theme)

You can use this theme on your Jekyll blog. This theme was primarily designed to be used in the HackPOD blog.
Hence, when you clone this theme HackPOD blog posts will also be cloned. To have your own posts, remove the posts HackPOD blog posts latter and replace them with your own. 

PS: It is recommended you understand how this theme works with the example posts before hacking this jekyll theme.

```
To see a demo
- Clone the Repo
- jekyll serve /path-to-cloned-repo/
- Visit http://localhost:8080
```

```--update 15 April 2017 ```

# Welcome to the hackpod blog
![Hello](http://images.fastcompany.com/upload/hello-wordcloud.jpg)

hackPOD blog is intended to be a collection of projects, cool hacks and creative art written by makers and artists from around the globe. So have a cool story, write here.  Oh, we almost forgot to say, **hello there :)**

## How to write to the hackpod blog?
- Fork / clone this repository
- Go to gh-pages branch.
- Go to **_posts/** folder.
- Write a post in markdown!
- Generate a pull request

## Umm, can you explain a bit furthur?
- Fork this repository by clicking **Fork** button.
- Now, in your forked repository navigate to **_posts/** folder.
- Now, create a post by clicking **+** button.
- Name the file in the format ```YYYY-MM-DD-post-title.md``` (example : ```2012-12-24-the-end-of-earth.md```)
- Write in markdown and preview changes as you write. If you dont know markdown, [visit here](http://markdowntutorial.com)
- when you're done press the **commit changes** green button.
- Next, create a pull request to hackpod/blog repository.
- We'll publish it then and there.

### OR
- Clone the repo.
- Create a post locally using any text editor.
- git push to your repo.
- Generate a pull request.
- We'll publish it then and there.

## Important stuff
- The post name should be of the format ``` YYYY-MM-DD-post-title.md ``` for jekyll to do its magic.
- Every post should have this section at the beginning :
```
---
layout: post
title: What's Jekyll?
date: 2012-02-06
author: **Insert your name**
author_image: **Insert your profile pic**
author_url: **Insert your url**
tags: **ONLY use any tag from: hackpod arduino make photography random campaign indiegogo**
        You can use any number of tags.
image: **Insert featured post image** 
---
```
- That's all folks.

### Feel free to talk to us on [talkPOD](https://gitter.im/hackpod/talkpod#).

## An Example post
```
---
layout: post
title: What's Jekyll?
date: 2012-02-06
author: Avikalpa Kundu
author_image:
author_url: http://avikalpa.github.io/
tags: 'hackpod'
image: http://blog.arduino.cc/wp-content/uploads/2015/06/Comelicottero-1024x682.jpg 
---

[Jekyll](http://jekyllrb.com) is a static site generator, an open-source tool for creating simple yet powerful websites of all shapes and sizes.</excerpt> From [the project's readme](https://github.com/mojombo/jekyll/blob/master/README.markdown):

  > Jekyll is a simple, blog aware, static site generator. It takes a template directory [...] and spits out a complete, static website suitable for serving with Apache or your favorite web server. This is also the engine behind GitHub Pages, which you can use to host your project�s page or blog right here from GitHub.

It's an immensely useful tool and one we encourage you to use here with Hyde.

Find out more by [visiting the project on GitHub](https://github.com/mojombo/jekyll).
```